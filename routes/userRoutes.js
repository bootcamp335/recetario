// routes/userRoutes.js
const express = require('express');
const router = express.Router();
const {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById
} = require('../controllers/userController');

// Crear un nuevo usuario
router.post('/new_user', createUser);

// Obtener todos los usuarios
router.get('/', getAllUsers);

// Obtener un usuario por ID
router.get('/:id', getUserById);

// Actualizar un usuario por ID
router.put('/:id', updateUserById);

// Eliminar un usuario por ID
router.delete('/:id', deleteUserById);

module.exports = router;
