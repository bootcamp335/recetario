// routes/homeRoutes.js
const express = require('express');
const router = express.Router();

// Ruta Home
router.get('/', (req, res) => {
    res.send('Bienvenido a la API de Recetas');
});

module.exports = router;