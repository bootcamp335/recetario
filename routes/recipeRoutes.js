// routes/recipeRoutes.js
const express = require('express');
const router = express.Router();
const {
    createRecipe,
    getAllRecipes,
    rateRecipe,
    getRecipesByUser,
    getRecipesByIngredient
} = require('../controllers/recipeController');

// Crear una nueva receta
router.post('/new_recipe', createRecipe);

// Obtener todas las recetas
router.get('/recipes', getAllRecipes);

// Calificar una receta
router.post('/rate', rateRecipe);

// Obtener recetas por usuario
router.get('/recipes_by_user', getRecipesByUser);

// Obtener recetas por ingrediente
router.get('/recipes_by_ingredient', getRecipesByIngredient);

module.exports = router;
