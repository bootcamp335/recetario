// controllers/recipeController.js
const recipeSchema = require('../models/recipe');

// Crear una nueva receta
const createRecipe = (req, res) => {
    const recipe = new recipeSchema(req.body);
    recipe.save()
        .then(data => res.json(data))
        .catch(error => res.json({ message: error }));
};

// Obtener todas las recetas
const getAllRecipes = (req, res) => {
    recipeSchema.find()
        .then(data => res.json(data))
        .catch(error => res.json({ message: error }));
};

// Calificar una receta
const rateRecipe = (req, res) => {
    const { recipeId, userId, rating } = req.body;
    recipeSchema.updateOne(
        { _id: recipeId },
        [
            { $set: { ratings: { $concatArrays: [{ $ifNull: ['$ratings', []] }, [{ userId, rating }] ] } } },
            { $set: { avgRating: { $trunc: [{ $avg: '$ratings.rating' }, 1] } } }
        ]
    )
    .then(data => res.json(data))
    .catch(error => res.json({ message: error }));
};

// Obtener recetas por usuario
const getRecipesByUser = (req, res) => {
    const { userId } = req.body;
    if (userId) {
        recipeSchema.find({ userId })
            .then(data => res.json(data))
            .catch(error => res.json({ message: error }));
    } else {
        res.send('Ingrese un ID de Usuario válido');
    }
};

// Obtener recetas por ingrediente
const getRecipesByIngredient = (req, res) => {
    const { ingredientsArray } = req.body;
    if (ingredientsArray) {
        recipeSchema.find({ "ingredients.name": { $in: ingredientsArray } })
            .then(data => res.json(data))
            .catch(error => res.json({ message: error }));
    } else {
        res.send('Ingrese una lista de ingredientes');
    }
};

module.exports = {
    createRecipe,
    getAllRecipes,
    rateRecipe,
    getRecipesByUser,
    getRecipesByIngredient
};
