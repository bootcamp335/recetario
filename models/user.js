const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

// Sub-schema para roles
const roleSchema = mongoose.Schema({
    type: {
        type: String,
        required: true,
        enum: ['admin', 'vendedor', 'cajero', 'cliente'],
        default: 'cliente'
    },
    permissions: [{
        type: String,
        required: true,
        enum: [
            'create_recipe',
            'edit_recipe',
            'delete_recipe',
            'manage_users',
            'view_sales',
            'process_sales'
        ]
    }]
}, { _id: false });

// Schema principal de usuario
const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        lowercase: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        match: [/^\S+@\S+\.\S+$/, 'Por favor ingrese un email válido']
    },
    password: {
        type: String,
        required: true,
        minlength: [6, 'La contraseña debe tener al menos 6 caracteres']
    },
    roles: {
        type: [roleSchema],
        default: [{ type: 'cliente', permissions: ['create_recipe'] }]
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

// Middleware para hashear el password antes de guardar
userSchema.pre('save', async function(next) {
    if (!this.isModified('password')) return next();
    
    try {
        const salt = await bcrypt.genSalt(10);
        this.password = await bcrypt.hash(this.password, salt);
        next();
    } catch (error) {
        next(error);
    }
});

// Método para comparar passwords
userSchema.methods.comparePassword = async function(candidatePassword) {
    return bcrypt.compare(candidatePassword, this.password);
};

module.exports = mongoose.model('User', userSchema);