const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const recipeRoutes = require('./routes/recipeRoutes');
const homeRoutes = require('./routes/homeRoutes');
const app = express();

// Middleware
app.use(express.json());
require('dotenv').config();

// Configuración CORS
const corsOptions = {
    origin: ['http://localhost:5173', 'http://localhost:3000'],
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true
};
app.use(cors(corsOptions));

// Conexión a MongoDB Atlas
mongoose.connect(process.env.DB_URI)
    .then(() => console.log("Conectado a MongoDB Atlas"))
    .catch(error => console.log("Error de Conexion: ", error));

// Rutas
app.use('/', homeRoutes);
app.use('/api/users', userRoutes);
app.use('/api/recipes', recipeRoutes);

app.listen(3000, () => console.log("Escuchando el puerto 3000..."));